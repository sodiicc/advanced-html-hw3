class Hamburger {
  constructor(size, stuffing) {
    try {
      if (size === undefined) {
        throw new Error('Enter correct Hamburger.size pls !')
      }
      this._size = size;
      if (stuffing === undefined) {
        throw new Error('Enter correct Hamburger.stuffing pls !')
      }
      this._stuffing = stuffing;
    } catch (e) {
      console.error(e)
    }

    this._topping = [];
  }

  get topping() {
    console.log(this._topping)
  }

  set topping(topp) {
    try {
      if (!topp) {
        throw new Error('Enter correct topping pls');
      }
  
      if (this._topping.find((x) => x == topp)) {
        throw new Error('You have already have added this topping !')
      }
      this._topping.push(topp);
    } catch (e) {
      console.error(e)
    }
  }

  removeTopping(topp) {
    try {
      if (!topp) {
        throw new Error('Enter correct topping pls');
      }
  
      let index = this._topping.indexOf(topp);
      if (index < 0) {
        throw new Error('This topping did not add!')
      }    
      this._topping.splice(index, 1);    
    } catch (e) {
      console.error(e)
    }    
  }

  get getName() {
    console.log(this._size.name)
  }

  calculatePrice() {
    try{
      let toppingPrice = 0;
      let price = 0;
    
      this._topping.forEach((el) => {
    
        if (typeof el.price !== 'number') {
          throw new Error('Enter correct topping pls !')
        }
        toppingPrice += el.price;
      })
      if (!this._size || !this._stuffing) {
        throw new Error('Enter correctly value of Hamburger !!!')
      }      
      price += (this._size.price + this._stuffing.price + toppingPrice);
      return price
    }catch(e){
      console.error(e)
    }
  };

  calculateCal() {

    try{
      let toppingCal = 0;
      let cal = 0;
    
      this._topping.forEach((el) => {
    
        if (typeof el.cal !== 'number') {
          throw new Error('Enter correct topping pls !')
        }
        toppingCal += el.cal;
      })
      if (!this._size || !this._stuffing) {
        throw new Error('Enter correctly value of Hamburger !!!')
      }      
      cal += (this._size.cal + this._stuffing.cal + toppingCal);
      return cal
    }catch(e){
      console.error(e)
    }
  }
}

Hamburger.SIZE_SMALL = {
  price: 50,
  cal: 20,
  size: 'small',
  name: 'SIZE_SMALL'
}
Hamburger.SIZE_LARGE = {
  price: 100,
  cal: 40,
  size: 'large',
  name: 'SIZE_LARGE'
}
Hamburger.STUFFING_CHEESE = {
  price: 10,
  cal: 20,
}
Hamburger.STUFFING_SALAD = {
  price: 20,
  cal: 5,
}
Hamburger.STUFFING_POTATO = {
  price: 15,
  cal: 10,
}
Hamburger.TOPPING_MAYO = {
  price: 15,
  cal: 0,
}
Hamburger.TOPPING_SPICE = {
  price: 20,
  cal: 5,
}

let ham = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO);

ham.topping = Hamburger.TOPPING_MAYO;
ham.topping = Hamburger.TOPPING_MAYO;
ham.topping = Hamburger.TOPPING_SPICE; /* Don't add the same topping */
ham.removeTopping(Hamburger.TOPPING_MAYO);
ham.topping
ham.getName
ham.getName

console.log('ham', ham);
console.log('ham.calculatePrice', ham.calculatePrice());
console.log('ham.calculateCal', ham.calculateCal());